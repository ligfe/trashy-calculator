import React from 'react';
import {SafeAreaView, StatusBar} from 'react-native';
import CalculatorScreen from './screens/index';

const App = () => {
  return (
    <SafeAreaView style={{backgroundColor: 'black'}}>
      <StatusBar barStyle={'light-content'} />
      <CalculatorScreen></CalculatorScreen>
    </SafeAreaView>
  );
};

export default App;