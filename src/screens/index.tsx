import React, {useState} from 'react';
import {StyleSheet, View} from 'react-native';
import {Button, Output} from '../component/index';

// Even though I couldn't have done the landscape, the portrait is pretty similiar. I'd say.
const CalculatorScreen = () => {

  // Every single one of 'em are used.
  // I know, I know.
  const [value, setValue] = useState('0');
  const [secValue, setSecValue] = useState('');
  const [operation, setOperation] = useState('');
  const [checker, setChecker] = useState(true);
  const [cond, setCond] = useState(true);
  var valueHolder, secValueHolder;

  // Set the values 0
  // or ''
  // or null.
  const clear = () => {
    setValue('0');
    setSecValue('');
    setOperation('');
    setChecker(true);
    setCond(true);
    valueHolder = null;
    secValueHolder = null;
  };

  // Get the value.
  const getValue = digit => {
    if (operation == '') {
      setValue(value === '0' ? digit : value + digit);
    } else {
      // 
      setCond(true);
      //
      setChecker(false);
      setSecValue(secValue + digit);
      secValueHolder = secValue;
    }
  };

  // Switch the sign between '-' and '+'.
  const onPlusMinus = () => {
    if (operation == '') {
      setValue(value !== '0' ? value * -1 : value);
    } else {
      setSecValue(secValue !== '' ? secValue * -1 : secValue);
    }
  };

  // Get one percent of the value.
  const onPercent = () => {
    if (operation == '') {
      setValue(value !== '0' ? value * 0.01 : value);
    } else {
      // It calculates your second input percent of your first input.
      // Example: 200 + 20 % => 40
      setSecValue((value / 100) * secValue);
    }
  };

  //  Put a dot in value.
  const onDot = () => {
    if (operation == '') {
      setValue(value.includes('.') ? value : value + '.');
    } else if (secValue == '') {
      setSecValue('0.');
    } else {
      setSecValue(secValue.includes('.') ? secValue : secValue + '.');
    }
  };

  // Get the sign.
  const getOperation = operator => {
    if (operation == '') {
      setOperation(operator);
    } else {
      setOperation(operator);
      setValue(secValue);
      setSecValue('');
    }
  };

  // Calculate the given equation.
  const onEqual = () => {  

    // I know it's messy and I'm sorry. Honestly I won't understand how it's working by the time I code it again lmao.
    // I'd be able to explain the code if we were together. 
    valueHolder = value;

    //  Summary
    if (operation == '+') {
      if (secValue == '') {
        setSecValue(parseFloat(value) + parseFloat(valueHolder));
      } else if (cond == true) {
        secValueHolder = secValue;
        setSecValue(parseFloat(value) + parseFloat(secValue));
        setValue(secValueHolder);
        setCond(false);
      } else {
        setSecValue(parseFloat(value) + parseFloat(secValue));
      }
    } else if (operation == '-') {
      if (secValue === '') {
        setSecValue(parseFloat(value) - parseFloat(valueHolder));
      } else if (checker == true) {
        setSecValue(parseFloat(secValue) - parseFloat(valueHolder));
      } else if (cond == true) {
        secValueHolder = secValue;
        setSecValue(parseFloat(value) - parseFloat(secValue));
        setValue(secValueHolder);
        setCond(false);
      } else {
        setSecValue(parseFloat(secValue) - parseFloat(value));
      }
    } else if (operation == '/') {
      if (secValue == '') {
        setSecValue(parseFloat(value) / parseFloat(valueHolder));
      } else if (checker == true) {
        setSecValue(parseFloat(secValue) / parseFloat(valueHolder));
      } else if (cond == true) {
        secValueHolder = secValue;
        setSecValue(parseFloat(value) / parseFloat(secValue));
        setValue(secValueHolder);
        setCond(false);
      } else {
        setSecValue(parseFloat(secValue) / parseFloat(value));
      }
    } else if (operation == '*') {
      if (secValue == '') {
        setSecValue(parseFloat(value) * parseFloat(valueHolder));
      } else if (cond == true) {
        secValueHolder = secValue;
        setSecValue(parseFloat(value) * parseFloat(secValue));
        setValue(secValueHolder);
        setCond(false);
      } else {
        setSecValue(parseFloat(value) * parseFloat(secValue));
      }
    }
  };

  return (
    <View style={styles.container}>
      <Output>{operation == '' ? value : secValue}</Output>
      <View style={{flex: 7}}>
        <View style={styles.input}>
          <View style={styles.buttonContainer}>
            {value == '0' && operation == '' ? (
              <Button
                title={'AC'}
                color="lightgray"
                textColor="black"
                onPress={() => {
                  clear();
                }}
              />
            ) : (
              <Button
                title={'C'}
                color="lightgray"
                textColor="black"
                onPress={() => {
                  clear();
                }}
              />
            )}
            <Button
              title={'+/-'}
              onPress={() => {
                onPlusMinus();
              }}
              color="lightgray"
              textColor="black"
            />
            <Button
              title={'%'}
              onPress={() => {
                onPercent();
              }}
              color="lightgray"
              textColor="black"
            />
            <Button
              title={'÷'}
              onPress={() => {
                getOperation('/');
              }}
              color="orange"
            />
          </View>
          <View style={styles.buttonContainer}>
            <Button
              title={7}
              onPress={() => {
                getValue('7');
              }}
            />
            <Button
              title={8}
              onPress={() => {
                getValue('8');
              }}
            />
            <Button
              title={9}
              onPress={() => {
                getValue('9');
              }}
            />
            <Button
              title={'×'}
              onPress={() => {
                getOperation('*');
              }}
              color="orange"
            />
          </View>
          <View style={styles.buttonContainer}>
            <Button
              title={4}
              onPress={() => {
                getValue('4');
              }}
            />
            <Button
              title={5}
              onPress={() => {
                getValue('5');
              }}
            />
            <Button
              title={6}
              onPress={() => {
                getValue('6');
              }}
            />
            <Button
              title={'−'}
              onPress={() => {
                getOperation('-');
              }}
              color="orange"
            />
          </View>
          <View style={styles.buttonContainer}>
            <Button
              title={1}
              onPress={() => {
                getValue('1');
              }}
            />
            <Button
              title={2}
              onPress={() => {
                getValue('2');
              }}
            />
            <Button
              title={3}
              onPress={() => {
                getValue('3');
              }}
            />
            <Button
              title={'+'}
              onPress={() => {
                getOperation('+');
              }}
              color="orange"
            />
          </View>
          <View style={styles.buttonContainer}>
            <Button
              title={0}
              onPress={() => {
                getValue('0');
              }}
              size="l"
            />
            <Button
              title={'.'}
              onPress={() => {
                onDot();
              }}
            />
            <Button
              title={'='}
              onPress={() => {
                onEqual();
              }}
              color="orange"
            />
          </View>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    height: '100%',
    backgroundColor: 'black',
  },
  output: {
    flex: 4,
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    paddingRight: 15,
  },
  value: {
    color: 'white',
    fontSize: 100,
    fontWeight: '200',
  },
  buttonContainer: {
    margin: 5,
    flexDirection: 'row',
    justifyContent: 'space-evenly',
  },
  input: {
    height: '100%',
  },
});

export default CalculatorScreen;
