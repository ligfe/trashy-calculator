import React from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  View,
  Text,
} from 'react-native';
import {colors} from './colors';

export const Button: React.FC<Props> = ({
  size,
  color,
  textColor,
  onPress,
  title,
}) => {
  const styles = StyleSheet.create({
    button: {
      height: 80,
      borderRadius: 50,
      alignItems: 'center',
      width: size === 'l' ? 172 : 80,
      backgroundColor: color ? colors[color] : colors.gray,
    },
    buttonValue: {
      flex: 1,
      paddingTop: 20,
      fontSize: 35,
      alignItems: 'center',
      fontWeight: '400',
      color: textColor ? textColor : 'white',
    },
  });

  return (
    <TouchableOpacity onPress={() => onPress()}>
      <View style={styles.button}>
        <Text style={styles.buttonValue}>{title}</Text>
      </View>
    </TouchableOpacity>
  );
};

type Props = {
  size?: 'l' | 's';
  color?: 'lightgray' | 'orange' | 'gray' | 'darkgray';
  textColor?: 'white' | 'black';
  onPress: Function;
  title?: any;
};
