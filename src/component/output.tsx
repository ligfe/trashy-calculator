import React from 'react';
import {StyleSheet, View, Text} from 'react-native';

export const Output: React.FC<Props> = ({children}) => {
  const styles = StyleSheet.create({
    output: {
      flex: 4,
      justifyContent: 'flex-end',
      alignItems: 'flex-end',
      paddingHorizontal: 10,
    },
    result: {
      color: 'white',
      fontSize: 80,       // originally 100,
      fontWeight: '200',
    },
  });

  return (
    <View style={styles.output}>
      <Text style={styles.result}>{children}</Text>
    </View>
  );
};

type Props = {
  children?: any;
};
